﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SistemaPisos.Models;
using SistemaPisos.Models.SQL;
using System.Collections;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data;
using System.Drawing;
using System.Web.Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Reflection;

namespace SistemaPisos.Controllers
{
    public class ReportingController : Controller
    {
        #region Recursos

        /// <summary>
        /// El error generado se envía a un array para darle manejo a la exepciones en el js
        /// </summary>
        /// <param name="ExMsj">Error</param>
        /// <returns></returns>
        public static string[] CapturarEx(string ExMsj)
        {
            string[] Ex = new string[2];

            Ex[0] = "ErrorEx";
            Ex[1] = ExMsj;

            return Ex;
        }

        /// <summary>
        /// Crea un elemento HTML para iTextSharp
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static Paragraph CreateSimpleHtmlParagraph(String text)
        {
            //Our return object
            Paragraph p = new Paragraph();

            //ParseToList requires a StreamReader instead of just text
            using (StringReader sr = new StringReader(text))
            {
                //Parse and get a collection of elements
#pragma warning disable CS0612 // Type or member is obsolete
                List<IElement> elements = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(sr, null);
#pragma warning restore CS0612 // Type or member is obsolete
                foreach (IElement e in elements)
                {
                    //Add those elements to the paragraph
                    p.Add(e);
                }
            }
            //Return the paragraph
            return p;
        }

        #endregion

        #region Utilidades

        /// <summary>
        /// Exportado de Histórico de Temperaturas y Volumen
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="FIni"></param>
        /// <param name="HIni"></param>
        /// <param name="FFin"></param>
        /// <param name="HFin"></param>
        /// <param name="Temp"></param>
        /// <param name="Vol"></param>
        public void ExportarExcel(string Eq, string Var, string FIni, string HIni, string FFin, string HFin, string NomVar)
        {
            DataAccess _da = new DataAccess();

            List<string> Datos = new List<string>();
            Datos.Add(Eq);
            Datos.Add(Var);
            Datos.Add(FIni.Replace("_", "/"));
            Datos.Add(HIni.Replace("_", ":"));
            Datos.Add(FFin.Replace("_", "/"));
            Datos.Add(HFin.Replace("_", ":"));
            Datos.Add("1");

            var Grid = new GridView();
            StringWriter sw = new StringWriter();
            HtmlTextWriter html_textWrite = new HtmlTextWriter(sw);

            html_textWrite.RenderBeginTag("p");
            html_textWrite.Write("<h2>REPORTE HISTORICO DE " + NomVar + "<h2>");
            html_textWrite.RenderEndTag();

            html_textWrite.AddStyleAttribute("font-size", "12pt");
            html_textWrite.AddStyleAttribute("color", "black");

            DataSet source = _da.SP_ConsultaHistoTempVolObjeto(Datos);

            if (source.Tables.Count > 0)
            {
                Grid.DataSource = source;
                Grid.DataBind();

                Color color = Color.FromArgb(255, 196, 23);
                Grid.HeaderStyle.BackColor = color;
                Grid.HeaderStyle.ForeColor = System.Drawing.Color.Black;

                Grid.RenderControl(html_textWrite);                
            }

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Historico.xls");
            Response.ContentType = "application/ms-excel";
            Response.Write(sw.ToString());
            Response.End();
        }

        /// <summary>
        /// Exportado de Histórico de Temperaturas y Volumen
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="FIni"></param>
        /// <param name="HIni"></param>
        /// <param name="FFin"></param>
        /// <param name="HFin"></param>
        /// <param name="Temp"></param>
        /// <param name="Vol"></param>
        public void AuditoriaTemperatura(string Fecha, string Eq, string Var, string NomEq)
        {
            DataAccess _da = new DataAccess();

            List<string> Datos = new List<string>();
            Datos.Add(Fecha.Replace("_", "/"));
            Datos.Add(Eq);
            Datos.Add(Var);

            DataSet source = _da.SP_AuditoriaTemperaturaEquipos(Datos);            

            Document document = new Document(PageSize.A4.Rotate());
            
            using (MemoryStream outputStream = new MemoryStream())
            {                
                dynamic w = PdfWriter.GetInstance(document, outputStream);

                document.Open();

                PdfPTable Table;
                PdfPCell cell;

                // Tabla Encabezado
                Table = new PdfPTable(5)
                {
                    WidthPercentage = 100f
                };

                Stream stmLogo = Assembly.GetExecutingAssembly().GetManifestResourceStream("SistemaPisos.Content.Images.Logo_Cliente.png");
                var imagenLogo = System.Drawing.Image.FromStream(stmLogo);
                var tifLogo = iTextSharp.text.Image.GetInstance(imagenLogo, System.Drawing.Imaging.ImageFormat.Jpeg);

                cell = new PdfPCell(tifLogo)
                {
                    PaddingTop = 3f,
                    PaddingBottom = 3f,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    FixedHeight = 65f,
                    Rowspan = 4
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("FORMATO SEGUIMIENTO TEMPERATURA EQUIPOS DE CONSERVACIÓN REFRIGERACIÓN", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    PaddingLeft = 40f,
                    PaddingRight = 40f,
                    Rowspan = 4,
                    Colspan = 3
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Código: PF07", FontFactory.GetFont(FontFactory.HELVETICA, 9)))
                {
                    Padding = 3f
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Versión: 02", FontFactory.GetFont(FontFactory.HELVETICA, 9)))
                {
                    Padding = 3f
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Última modificación: 07/08/2019", FontFactory.GetFont(FontFactory.HELVETICA, 9)))
                {
                    Padding = 3f
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Página: 1 de 1", FontFactory.GetFont(FontFactory.HELVETICA, 9)))
                {
                    Padding = 3f
                };
                Table.AddCell(cell);

                document.Add(Table);

                // Tabla ubicación
                Table = new PdfPTable(3)
                {
                    WidthPercentage = 100f
                };

                cell = new PdfPCell(new Paragraph("EQUIPO: " + NomEq, FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    PaddingTop = 10f,
                    Border = iTextSharp.text.Rectangle.NO_BORDER
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("ÁREA: Almacenamiento", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    PaddingTop = 10f,
                    Border = iTextSharp.text.Rectangle.NO_BORDER
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("ACTIVIDAD: Producción", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    PaddingTop = 10f,
                    Border = iTextSharp.text.Rectangle.NO_BORDER
                };
                Table.AddCell(cell);

                document.Add(Table);

                // Tabla Temperaturas e Imagen de gráfica                
                Table = new PdfPTable(4)
                {
                    WidthPercentage = 35f,
                    HorizontalAlignment = Element.ALIGN_LEFT,                    
                };

                float[] widths = new float[] { 10f, 8f, 8f, 12f };
                Table.SetWidths(widths);

                cell = new PdfPCell(new Paragraph("TEMPERATURA OBJETIVA: 0-6 °C", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    
                    PaddingTop = 10f,
                    PaddingBottom = 10f,
                    Colspan = 4,
                    Border = iTextSharp.text.Rectangle.BOTTOM_BORDER
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("FECHA", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    Padding = 3f,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("HORA", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    Padding = 3f,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("TEMP °C", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    Padding = 3f,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("OBSERVACIÓN", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    Padding = 3f,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                };
                Table.AddCell(cell);

                // Gráfica
                List<string> xValues = new List<string>();
                List<string> yValues = new List<string>();

                for (int i = 0; i < source.Tables.Count; i++)
                {
                    if (source.Tables[i].Rows.Count > 0)
                    {
                        xValues.Add((i + 1).ToString());
                        yValues.Add(source.Tables[i].Rows[0][1].ToString().Replace(",", "."));

                        cell = new PdfPCell(new Paragraph(source.Tables[i].Rows[0][3].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                        };
                        Table.AddCell(cell);

                        cell = new PdfPCell(new Paragraph(source.Tables[i].Rows[0][4].ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                        };
                        Table.AddCell(cell);

                        cell = new PdfPCell(new Paragraph(source.Tables[i].Rows[0][1].ToString().Replace(",", ".").Substring(0, 4), FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                        };
                        Table.AddCell(cell);

                        cell = new PdfPCell(new Paragraph((i + 1).ToString(), FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                        {
                            HorizontalAlignment = Element.ALIGN_CENTER,
                        };
                        Table.AddCell(cell);
                    }
                }

                float responableVerificaOffset = 0f;

                if (xValues.Count() > 0)
                {
                    document.Add(Table);

                    string t = @"<Chart BorderlineDashStyle=""Solid"" BorderWidth=""3""> <ChartAreas> <ChartArea Name=""Default"" _Template_=""All""> <AxisX Interval=""1""></AxisX> </ChartArea> </ChartAreas> </Chart>";

                    // Create bar chart
                    var chart = new Chart(width: 875, height: 350, theme: t)
                                    .AddSeries(chartType: "line",
                                               markerStep: 1,
                                               xValue: xValues.AsEnumerable(),
                                               yValues: yValues.AsEnumerable())
                                    .AddTitle("GRÁFICOS TEMPERATURA / TIEMPO")
                                    .GetBytes("png");


                    Stream stmGrafica = new MemoryStream(chart);

                    var imagenGrafica = System.Drawing.Image.FromStream(stmGrafica);
                    var tifGrafica = iTextSharp.text.Image.GetInstance(imagenGrafica, System.Drawing.Imaging.ImageFormat.Jpeg);

                    iTextSharp.text.Image objImagenGrafica;
                    objImagenGrafica = tifGrafica;

                    objImagenGrafica.Alignment = Element.ALIGN_LEFT;
                    objImagenGrafica.SetAbsolutePosition(300, 165);
                    objImagenGrafica.ScalePercent(60);
                    document.Add(objImagenGrafica);

                    responableVerificaOffset = (24f - xValues.Count()) * 12f;
                }
                else
                {
                    // Tabla Sin información para mostrar
                    Table = new PdfPTable(1)
                    {
                        WidthPercentage = 100f
                    };

                    cell = new PdfPCell(new Paragraph("No se encontró información para graficar en la fecha seleccionada", FontFactory.GetFont(FontFactory.HELVETICA, 20)))
                    {
                        PaddingTop = 165f,
                        PaddingBottom = 165f,
                        Border = iTextSharp.text.Rectangle.NO_BORDER,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                    };
                    Table.AddCell(cell);
              
                    document.Add(Table);
                }

                // Tabla Responsable / Verifica
                Table = new PdfPTable(5)
                {
                    WidthPercentage = 100f
                };

                cell = new PdfPCell(new Paragraph("RESPONSABLE: _______________________________", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    PaddingTop = 30f + responableVerificaOffset,
                    Border = iTextSharp.text.Rectangle.NO_BORDER,
                    Colspan = 2
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("VERIFICA: _______________________________", FontFactory.GetFont(FontFactory.HELVETICA, 10)))
                {
                    PaddingTop = 30f + responableVerificaOffset,
                    Border = iTextSharp.text.Rectangle.NO_BORDER,
                    Colspan = 2
                };
                Table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(""))
                {
                    PaddingTop = 15f,
                    Border = iTextSharp.text.Rectangle.NO_BORDER
                };
                Table.AddCell(cell);

                document.Add(Table);

                document.Close();
                
                Response.Clear();
                
                Response.ContentType = "application/pdf";
                
                Response.AddHeader("Expires", "0");
                Response.AddHeader("Cache-Control", "");
                
                Response.AddHeader("Content-Disposition", "attachment; filename=Reporte_" + Fecha.Replace("/", "_") + ".pdf");
                
                Response.AddHeader("Content-length", outputStream.GetBuffer().Length.ToString());
                
                Response.OutputStream.Write(outputStream.GetBuffer(), 0, outputStream.GetBuffer().Length);
                
                Response.End();
            }
        }

        #endregion

        #region ActionResult(HttpGet)

        public ActionResult Consultas()
        {
            return View();
        }

        #endregion

        #region ActionResult(HttpPost)

        /// <summary>
        /// Consulta de los maestros según las opciones enviadas
        /// </summary>
        /// <param name="_Maestro"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Consultar_Maestros(Maestro _Maestro)
        {
            string data = string.Empty;

            try
            {
                DataAccess _da = new DataAccess();

                data = JsonConvert.SerializeObject(_da.SP_Consultar_Maestros(_Maestro), Formatting.Indented);
            }
            catch (Exception ex)
            {
                data = JsonConvert.SerializeObject(CapturarEx(ex.Message), Formatting.Indented);
            }

            return Json(new { data });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Par"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ConsultaHistoTemperaturaVolumenObjeto(List<string> Datos)
        {
            string data = string.Empty;

            try
            {
                DataAccess _da = new DataAccess();

                data = JsonConvert.SerializeObject(_da.SP_ConsultaHistoTempVolObjeto(Datos), Formatting.Indented);
            }
            catch (Exception ex)
            {
                data = JsonConvert.SerializeObject(CapturarEx(ex.Message), Formatting.Indented);
            }

            return Json(new { data });
        }
        
        #endregion
    }
}