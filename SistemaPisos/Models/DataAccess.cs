﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SistemaPisos.Models.SQL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.Common;

namespace SistemaPisos.Models
{
    public class DataAccess
    {
        #region Conexión

        static Database _dataBase = new Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase(ConfigurationManager.ConnectionStrings["str_cnx_SistemaPiso"].ToString());

        #endregion

        #region Consultas

        [Description("SP para la consulta de maestros según las opciones enviadas")]
        public DataSet SP_Consultar_Maestros(Maestro _Maestro)
        {
            DataSet _dt = new DataSet();

            try
            {
                using (DbCommand dbCommand = _dataBase.GetStoredProcCommand("ConsultarMaestros"))
                {
                    _dataBase.AddInParameter(dbCommand, "Opcion", DbType.String, _Maestro.Opcion);
                    _dataBase.AddInParameter(dbCommand, "Valor", DbType.String, _Maestro.Valor);
                    _dataBase.AddInParameter(dbCommand, "Codigo", DbType.String, _Maestro.Codigo);

                    _dt = _dataBase.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _dt;
        }

        [Description("SP para la Temperatura y Volumen según sea la necesidad")]
        public DataSet SP_ConsultaHistoTempVolObjeto(List<string> Datos)
        {
            DataSet _dt = new DataSet();

            try
            {
                using (DbCommand dbCommand = _dataBase.GetStoredProcCommand("ConsultaHistoTempVolObjeto"))
                {
                    _dataBase.AddInParameter(dbCommand, "IdEquipo", DbType.Int32, int.Parse(Datos[0]));
                    _dataBase.AddInParameter(dbCommand, "IdVariable", DbType.Int32, int.Parse(Datos[1]));
                    _dataBase.AddInParameter(dbCommand, "FechaIni", DbType.String, Datos[2]);
                    _dataBase.AddInParameter(dbCommand, "HoraIni", DbType.String, Datos[3]);
                    _dataBase.AddInParameter(dbCommand, "FechaFin", DbType.String, Datos[4]);
                    _dataBase.AddInParameter(dbCommand, "HoraFin", DbType.String, Datos[5]);
                    _dataBase.AddInParameter(dbCommand, "Exportar", DbType.String, Datos[6]);

                    _dt = _dataBase.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _dt;
        }

        [Description("SP para la Auditoría de temperatura de Equipos")]
        public DataSet SP_AuditoriaTemperaturaEquipos(List<string> Datos)
        {
            DataSet _dt = new DataSet();

            try
            {
                using (DbCommand dbCommand = _dataBase.GetStoredProcCommand("AuditoriaTemperaturaEquipos"))
                {
                    _dataBase.AddInParameter(dbCommand, "Fecha", DbType.String, Datos[0]);
                    _dataBase.AddInParameter(dbCommand, "IdEquipo", DbType.Int32, int.Parse(Datos[1]));
                    _dataBase.AddInParameter(dbCommand, "IdVariable", DbType.Int32, int.Parse(Datos[2]));

                    _dt = _dataBase.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _dt;
        }

        #endregion
    }
}