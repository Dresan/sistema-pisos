﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class Objeto
    {
        public int ObjetoId { get; set; }
        public string Descripcion { get; set; }
        public int TipoObjetoId { get; set; }
        public bool Estado { get; set; }
    }
}