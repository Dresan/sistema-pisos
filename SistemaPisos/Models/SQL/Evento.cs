﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class Evento
    {
        public Int32 EventoId { get; set; }
        public string Descripcion { get; set; }
    }
}