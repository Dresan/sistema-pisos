﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class Encabezado
    {
        public Int64 EncabezadoId { get; set; }
        public Int32 ObjetoId { get; set; }
        public Int32 ProcesoId { get; set; }
        public string Fecha1 { get; set; }
        public string Fecha2 { get; set; }
        public string Observaciones1 { get; set; }
        public string Observaciones2 { get; set; }   
    }
}