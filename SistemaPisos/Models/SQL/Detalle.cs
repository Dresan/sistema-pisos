﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class Detalle
    {
        public Int64 DetalleId { get; set; }
        public Int64 EncabezadoId { get; set; }
        public Int32 ResponsableId { get; set; }
        public Int32 EventoId { get; set; }
        public Int32 ProcesoId { get; set; }
        public string UnidadMedida { get; set; }
        public decimal ValorNumerico { get; set; }
        public string ValorTexto { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public string Almacenamiento { get; set; }
        public string Muelle { get; set; }
        public string Compartimiento { get; set; }
        public string Fecha1 { get; set; }
        public string Fecha2 { get; set; }
        public string Observaciones1 { get; set; }
        public string Observaciones2 { get; set; }
    }
}