﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class TipoObjeto
    {
        public Int32 TipoObjetoId { get; set; }
        public string Descripcion { get; set; }
    }
}