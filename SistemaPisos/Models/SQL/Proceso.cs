﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class Proceso
    {
        public Int32 ProcesoId { get; set; }
        public string Descripcion { get; set; }
    }
}