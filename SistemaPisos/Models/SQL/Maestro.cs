﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaPisos.Models.SQL
{
    public class Maestro
    {
        public string Opcion { set; get; }
        public string Valor { set; get; }
        public string Codigo { set; get; }
    }
}