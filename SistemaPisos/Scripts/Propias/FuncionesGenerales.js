﻿/// <reference path="../bootstrap/js/2.0.3/jquery-2.0.3.min.js" />

var par, url, resp; //Variables para petición al ajax al servidor
var Select2Timer;   //Timer que activa el momento de ir al servidor

$(document).ready(function () {

    setSoloNumeros();
    setSoloNumerosDecimal();
    //setDatePicker();
    setSelect2();
    //setFormatoMoneda();
    //setFormatoMonedaDecimal();
});

//************************************************************************
//Ajax
function ajax(par, url) {
    return jQuery.ajax({
        url: url,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: par,
        cache: false,
        beforeSend: function () {

            swal({
                title: "<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom'></i>",
                html: "Cargando...",
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false
            });
        }
    });
}
//************************************************************************

//************************************************************************
//Ajax No Asyncrono
function ajaxNA(par, url) {
    return jQuery.ajax({
        url: url,
        type: "POST",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: par,
        cache: false
    });
}
//************************************************************************

//************************************************************************
//Formato Pesos
function FormatoPesos(num, input) {
    return "$" + num
       .toFixed(0)
       .replace(".", ",")
       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}
//************************************************************************

//************************************************************************
//Valida si la información enviada desde el servidor no es un error
function ValidarDataSA(data) {

    swal.close();

    var Resp = false;

    if (data != null && data[0] === "ErrorEx")
        MostrarMsjSA(1, data[1]);
    else
        Resp = true;    

    return Resp;
}
//************************************************************************

//************************************************************************
//Solo números en un campo de texto
function setSoloNumeros() {
    $(".SoloNumeros").keypress(function (e) {

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }

    });
}
//************************************************************************

//************************************************************************
//Solo números en un campo de texto
function setSoloNumerosDecimal() {
    $(".SoloNumerosDecimal").keypress(function (e) {

        if (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }

    });
}
//************************************************************************

//************************************************************************
//Asignar datepicker 
function setDatePicker() {
    $(".DatePicker").datepick({
        dateFormat: 'dd/mm/yyyy'
    });
}
//************************************************************************

//************************************************************************
//Captura una hora con formato de 24 y la transforma en formato de 12 
function FormatHora12(Hora) {
    if (Hora != null && Hora != String()) {
        var Hour = Hora.split(":")[0];
        var DD = "AM";
        var newHour;

        if (Hour > 12) {
            Hour = Hour - 12;
            DD = "PM";

            if (Hour.toString().length == 1)
                Hour = "0" + Hour;
        }

        newHour = Hour + ":" + Hora.split(":")[1] + ":" + Hora.split(":")[2] + " " + DD;

        return newHour;
    }
}
//************************************************************************

//************************************************************************
//Dar formato a la fecha que se envía desde el servidor para mostrarla como dd/MM/yyyy
//Formato del Parametro: 2015-05-25T00:00:00.000
function FormatFechaDMY(Fecha) {

    var Date = String();

    if (Fecha != null && Fecha != undefined) {

        Fecha = Fecha.split('T')[0];

        Date = Fecha.split('-')[2] + "/" + Fecha.split('-')[1] + "/" + Fecha.split('-')[0];
    }

    return Date;
}
//************************************************************************

//************************************************************************
//Asigna el estilo de Select2 a un combo
function setSelect2() {
    $(".Select2").select2({
        width: "100%",
        page_limit: 10 
    });
}
//************************************************************************

//************************************************************************
//Asigna el estilo de Select2 con timer para ir al servidor (Se debe asignar a un txt)
//opc: Formato del data 1:Consultar Maestro Viáticos
function setSelect2Timer(IdTxt, par, url, opc) {

    $("#" + IdTxt).select2({
        placeholder: "Seleccione",
        minimumInputLength: 3,
        query: function (query) {
            if (!(Select2Timer === undefined))
                clearInterval(Select2Timer);

            Select2Timer = setInterval(function () {

                clearInterval(Select2Timer);

                var data = { results: [] };
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: opc == 1 ? "{'Par':[{'Opcion':'" + par[0] + "','Codigo':'" + jQuery('.select2-active').val() + "','Valor':'" + par[2] + "'}]}" : "", //Parámetros
                    url: url, //URL del método
                    dataType: "json",
                    success: function (datas) {

                        datas = $.parseJSON(datas.d);

                        for (var i = 0; i < datas.Table.length; i++) {
                            data.results.push({ id: datas.Table[i].COD, text: datas.Table[i].NOM });
                        }
                        query.callback(data);

                        clearInterval(Select2Timer);
                    },
                    error: function (result) {
                    }
                });
            }, 400);

        },

        // formatResult: ResultadoEmpleados, // omitted for brevity, see the source of this page
        formatSelection: Seleccion,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        allowClear: true
    });

    function Seleccion(obj, item) {
        return obj.text;
    }
}
//************************************************************************

//************************************************************************
//Abrir Modal
function AbrirModal(IdModal) {
    $("#" + IdModal).modal("show").appendTo("body");
}
//************************************************************************

//************************************************************************
//Cerrar Modal
function CerrarModal(IdModal) {
    $("#" + IdModal).modal("hide");
}
//************************************************************************

//************************************************************************
//Obtener solo los número de un string
function ObtenerSoloNumero(Value) {
    var numb = Value.match(/\d/g);
    numb = numb.join("");

    return numb;
}
//************************************************************************

//************************************************************************
//Captura el máximo valor de un array numerico (array.max())
Array.prototype.max = function () {
    return Math.max.apply(null, this);
};
//************************************************************************

//************************************************************************
//Captura el mínimo valor de un array numerico (array.min())
Array.prototype.min = function () {
    return Math.min.apply(null, this);
};
//************************************************************************

//************************************************************************
//Valida si un email tiene el formato correcto
function ValidarEmail(email) {
    var Resp = true;

    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!expr.test(email))
        Resp = false;

    return Resp;
}
//************************************************************************

//************************************************************************
//Según el número del mes enviado retorna su correspondiene en letras
function RetornarMes(Mes) {

    switch (Mes) {
        case 1:
            Mes = "Enero";
            break;
        case 2:
            Mes = "Febrero";
            break;
        case 3:
            Mes = "Marzo";
            break;
        case 4:
            Mes = "Abril";
            break;
        case 5:
            Mes = "Mayo";
            break;
        case 6:
            Mes = "Junio";
            break;
        case 7:
            Mes = "Julio";
            break;
        case 8:
            Mes = "Agosto";
            break;
        case 9:
            Mes = "Septiembre";
            break;
        case 10:
            Mes = "Octubre";
            break;
        case 11:
            Mes = "Noviembre";
            break;
        case 12:
            Mes = "Diciembre";
            break;
        default:
            Mes = "Sin Identificar";
    }

    return Mes;
}
//************************************************************************

//************************************************************************
//Cuando se esta digitando un valor de dinero en un input se agregan los puntos de miles y el simbolo de $
function setFormatoMoneda() {

    var currency = $(".moneda");

    currency.keydown(function (e) {
        if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)
            && e.keyCode != 8
            && e.keyCode != 46
            && e.keyCode != 40
            && e.keyCode != 39
            && e.keyCode != 38
            && e.keyCode != 37
            )
            return false;
    });

    currency.keyup(function (e) {
        if (!($(e.target).val() == "$" || e.keyCode == 46
           || e.keyCode == 40
           || e.keyCode == 39
           || e.keyCode == 38
           || e.keyCode == 37
           ))
            $(e.target).val($(e.target).val() === '' ? '' : FormatoPesos(parseFloat($(e.target).val().split(".").join("").replace("$", ""))));
    });

    currency.focusout(function (e, i) {
        $(e.target).val($(e.target).val().replace("$", ""));
        if ($(e.target).val().indexOf("NaN") != -1) {
            $(e.target).val("");
        }
        $(e.target).val($(e.target).val() === '' ? '' : FormatoPesos(parseFloat($(e.target).val().split(".").join("").replace("$", ""))));
    });

    currency.focusin(function (e, i) {
        $(e.target).val($(e.target).val().replace("$", ""));
    });
}
//************************************************************************

//************************************************************************
//Mensajes de alerta con librería SweetAlert
//1:Error
//2:Éxito
//3:Info
//4:Warning
function MostrarMsjSA(Opcion, Msj) {

    if (Opcion == 1)  //Error
        swal({
            title: "Atención!",
            text: Msj,
            type: "error",
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    else if (Opcion == 2) //Éxito
        swal({
            title: "Éxito!",
            text: Msj,
            type: "success",
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    else if (Opcion == 3) //Info
        swal({
            title: "Atención!",
            text: Msj,
            type: "info",
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    else if (Opcion == 4) //Warning
        swal({
            title: "Atención!",
            text: Msj,
            type: "warning",
            allowOutsideClick: false,
            allowEscapeKey: false
        });
}
//************************************************************************

//************************************************************************
//Cuando se esta digitando un valor de dinero en un input se agregan los puntos de miles y el simbolo de $
function setFormatoMonedaDecimal() {

    var currency = $(".MonedaDecimal");

    currency.keydown(function (e) {
        if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)
            && e.keyCode != 8
            && e.keyCode != 46
            && e.keyCode != 40
            && e.keyCode != 39
            && e.keyCode != 38
            && e.keyCode != 37
            && e.keyCode != 190
            && e.keyCode != 110
            )
            return false;
    });

    currency.focusout(function (e, i) {
        $(e.target).val($(e.target).val().replace("$", ""));
        if ($(e.target).val().indexOf("NaN") != -1) {
            $(e.target).val("");
        }
        $(e.target).val($(e.target).val() === '' ? '' : FormatoPesosDecimal(parseFloat($(e.target).val().split(",").join("").replace("$", ""))));
    });

    currency.focusin(function (e, i) {
        $(e.target).val($(e.target).val().split(",").join("").replace("$", ""));
    });
}
//************************************************************************

//************************************************************************
//Formato Pesos Decimal
function FormatoPesosDecimal(num, input) {
    return "$" + parseFloat(num)
       .toFixed(2)
       //.replace(".", ",")
       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
//************************************************************************

//************************************************************************
//Formato Decimal 
function FormatoDecimal(num) {
    return parseFloat(num)
       .toFixed(2)
       //.replace(".", ",")
       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
//************************************************************************

//************************************************************************
//Obenter paramaetros por URL
//Se debe enviar el nombre del parametro de la URL
function getUrlParameter(name, url) {
    
    if (!url) url = window.location.href;

    name = name.replace(/[\[\]]/g, "\\$&");

    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);

    if (!results) return null;

    if (!results[2]) return '';

    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
//************************************************************************

//************************************************************************
//Exportar tabla excel js
//Se debe crear el objeto "dlink" -->   <a id="dlink"  style="display:none;"></a>
TableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><?xml version="1.0" encoding="UTF-8" standalone="yes"?><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name, filename) {

        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

        document.getElementById("dlink").href = uri + base64(format(template, ctx));
        document.getElementById("dlink").download = filename + '.xls';
        document.getElementById("dlink").click();

        return false;
    }
})();
//************************************************************************

//************************************************************************
//Validar Sesión SweetAlert: Valida que la sesión este activa la momento de realizar la petición al servidor
//pag: Págia
//f: Función a ejecutar
function ValidarSessionSA(pag, f) {

    swal({
        title: "<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom'></i>",
        html: "Cargando...",
        showConfirmButton: false,
        allowEscapeKey: false,
        allowOutsideClick: false
    });

    par = String();
    url = pag + "/ValidarSession";

    resp = ajax(par, url);

    resp.done(function (data) {        

        data = $.parseJSON(data.d);

        if (data != null) {
            if (data[0].Status === "0") {
                window.location = "http://sabemas.colanta.com.co/";
            }
            else {
                f();
            }
        }

    });
}
//************************************************************************

//************************************************************************
//Evitar Pegar en un campo del formuario
function evitarPaste() {
    $('.evitarPaste').on('paste', function () {
        return false
    });
}
//************************************************************************

//************************************************************************
//
//************************************************************************

//************************************************************************
//
//************************************************************************

//************************************************************************
//
//************************************************************************

//************************************************************************
//
//************************************************************************