﻿/// <reference path="../../jQuery/jquery-3.1.1.min.js" />
/// <reference path="../../Knockout/knockout-3.4.1.js" />
/// <reference path="../../Propias/FuncionesGenerales.js" />

//#region Globales
var alreadyFetched = {};
var Data_Consulta = [];
var NumGraficas = 0;
var Plot_Consulta;
//#endregion

//#region Load
$(document).ready(function () {

    vm.ConsutarTiposEquipos();

    InicializarEventos();
});
//#endregion

//#region Inicializar Eventos
function InicializarEventos() {

    //#region DatePicker
    $('#txtFechaIni').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        showTodayButton: true,
        showClear: true,
        defaultDate: moment(new Date())
    });

    $('#txtFechaFin').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        showTodayButton: true,
        showClear: true,
        defaultDate: moment(new Date()),
        maxDate: moment(new Date())
    });
    //#endregion

    //#region TimePicker
    $('#txtHoraIni').datetimepicker({
        locale: 'es',
        format: 'HH:mm',
        showClear: true,
        defaultDate: moment(new Date()).hours(0).minutes(0).seconds(0).milliseconds(0)
    });

    $('#txtHoraFin').datetimepicker({
        locale: 'es',
        format: 'HH:mm',
        showClear: true,
        defaultDate: moment(new Date())
    });

    $('#txtFechaIni').val('04/09/2019');
    $('#txtFechaFin').val('04/09/2019');
    //#endregion
}
//#endregion

//#region ViewModel
function vmConsultas() {

    var self = this;

//#region Observables

    //#region Generales
    self.Opcion = ko.observable(0);
    //#endregion

    //#region Mediciones
    self.TipoEquipo = ko.observable("");
    self.Equipo = ko.observable("");
    self.Variable = ko.observable("");
    self.FechaIni = "";
    self.HoraIni = "";
    self.FechaFin = "";
    self.HoraFin = "";

    self.MsjZoom = ko.observable('');
    self.LimiteSup = ko.observable(null);
    self.LimiteInf = ko.observable(null);

    self.SWGrafico = ko.observable(false);
    self.SWZoom = ko.observable(false);

    self.aProcesos = ko.observableArray([]);
    self.aTipoEquipos = ko.observableArray([]);
    self.aEquipos = ko.observableArray([]);
    self.aVariables = ko.observableArray([]);
    //#endregion

//#endregion

//#region Funciones

    //#region Alert de cargando info
    self.CargarndoInfo = function () {

        swal({
            title: "<i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw margin-bottom'></i>",
            html: "Cargando...",
            showConfirmButton: false
        });
    }
    //#endregion

    //#region Consultar Tipos de Equipos
    self.ConsutarTiposEquipos = function () {

        var _Maestro = {

            Opcion: 'CTE'
        }

        par = "{'_Maestro': " + JSON.stringify(_Maestro) + "}";
        url = "/Reporting/Consultar_Maestros";

        resp = ajax(par, url);

        resp.done(function (data) {

            data = $.parseJSON(data.data);

            if (ValidarDataSA(data)) {

                if (data.Table.length > 0)
                    self.aTipoEquipos(data.Table);
                else
                    MostrarMsjSA(3, "No se han registrado Tipos de Equipos.");
            }
        });

        resp.fail(function (data) {

            MostrarMsjSA(1, "Error al ejecutar: ConsutarTiposEquipos --->" + data + $.parseJSON(data.responseText).Message);
        });
    }
    //#endregion

    //#region Consultar Equipos
    self.ConsutarEquipos = function () {

        self.aEquipos([]);

        if (self.TipoEquipo()) {

            var _Maestro = {

                Opcion: 'CEXT',
                Valor: self.TipoEquipo()
            }

            par = "{'_Maestro': " + JSON.stringify(_Maestro) + "}";
            url = "/Reporting/Consultar_Maestros";

            resp = ajax(par, url);

            resp.done(function (data) {

                data = $.parseJSON(data.data);

                if (ValidarDataSA(data)) {

                    if (data.Table.length > 0)
                        self.aEquipos(data.Table);
                    else
                        MostrarMsjSA(3, "No se han registrado Equipos para la medición.");
                }
            });

            resp.fail(function (data) {

                MostrarMsjSA(1, "Error al ejecutar: ConsutarEquipos --->" + data + $.parseJSON(data.responseText).Message);
            });
        }        
    }
    //#endregion

    //#region Consultar Variables del Equipo
    self.ConsutarVariablesEquipo = function () {

        self.aVariables([]);

        if (self.Equipo()) {

            var _Maestro = {

                Opcion: 'CVE',
                Valor: self.Equipo()
            }

            par = "{'_Maestro': " + JSON.stringify(_Maestro) + "}";
            url = "/Reporting/Consultar_Maestros";

            resp = ajax(par, url);

            resp.done(function (data) {

                data = $.parseJSON(data.data);

                if (ValidarDataSA(data)) {

                    if (data.Table.length > 0)
                        self.aVariables(data.Table);
                    else
                        MostrarMsjSA(3, "No se han registrado Variables para el Equipo Seleccionado.");
                }
            });

            resp.fail(function (data) {

                MostrarMsjSA(1, "Error al ejecutar: ConsutarVariablesEquipo --->" + data + $.parseJSON(data.responseText).Message);
            });
        }
    }
    //#endregion

    //#region Consultar Histórico
    self.ConsultarHistTempVol = function (item) {

        if (self.ValidarFormulario()) {

            var NomVariable = $("#ddlVariable").select2('data').text;
            var Parametros = [];
            var UnidadMedida = String();

            //Si no es Zoom se toman los datos normales
            if (!self.SWZoom()) {

                Parametros.push(self.Equipo().toString());
                Parametros.push(self.Variable().toString());
                Parametros.push(self.FechaIni);
                Parametros.push(self.HoraIni + ':00');
                Parametros.push(self.FechaFin);
                Parametros.push(self.HoraFin + ':00');
                Parametros.push("0");
            } else {

                Parametros.push(self.Equipo().toString());
                Parametros.push(self.Variable().toString());
                Parametros.push(item.FIF.split(" ")[0]);
                Parametros.push(item.FIF.split(" ")[1]);
                Parametros.push(item.FFF.split(" ")[0]);
                Parametros.push(item.FFF.split(" ")[1]);
                Parametros.push("0");

                self.MsjZoom(NomVariable + " del proceso " + item.Desc + " entre el " + item.FIF + " y el " + item.FFF);
            }

            par = JSON.stringify(Parametros);
            url = "/Reporting/ConsultaHistoTemperaturaVolumenObjeto";

            resp = ajax(par, url);

            resp.done(function (data) {

                data = $.parseJSON(data.data);

                if (ValidarDataSA(data)) {

                    if (data.Table.length > 0) {

                        UnidadMedida = data.Table[0].UM;

                        //Plot
                        var PlaceHolder = $("#PlotPlaceholder");
                        var OverView = $("#PlotOverView");

                        var options = {
                            legend: {
                                show: true
                            },
                            series: {
                                lines: {
                                    show: true
                                },
                                points: {
                                    show: true
                                }
                            },
                            //colors: ["#d41616"],
                            backgroundColor: "#ffffff",
                            yaxis: {
                                ticks: 10,
                                max: self.LimiteSup() == "" ? null : self.LimiteSup(),
                                min: self.LimiteInf() == "" ? null : self.LimiteInf()
                            },
                            xaxis: {
                                mode: "time",
                                timeformat: "%d/%m/%Y %H:%M:%S",
                                minTickSize: [1, "minute"],
                                timezone: "browser"
                            },
                            selection: {
                                mode: "x"
                            },
                            grid: {
                                hoverable: true,
                                clickable: true
                            }
                        };

                        //Data
                        var Series = [];                        
                        
                        //Creación de Series
                        $.each(data.Table, function (i, j) {

                            Series.push([new Date(j.FC).getTime(), j.VLR]);
                        });                        

                        //Series
                        Series = {
                            "label": $("#ddlEquipo").select2("data").text + " - " + $("#ddlVariable").select2("data").text,
                            "data": Series
                        }

                        //Añado al array la serie seleccionada y la verifico para solo adicionarla una vez
                        if (!alreadyFetched[self.Equipo() + "_" + self.Variable()]) {

                            alreadyFetched[self.Equipo() + "_" + self.Variable()] = true;
                            Data_Consulta.push(Series);
                            NumGraficas++;
                        } else {
                            
                            //Si solo se a agregado una gráfica y es la misma se reemplaza con los nuevos rangos
                            if (NumGraficas == 1) {

                                Data_Consulta = [];
                                Data_Consulta.push(Series);
                            } else {

                                MostrarMsjSA(3, "Para agregar nuevamente un Equipo con diferentes valores en rangos de Fecha y Hora debe realizar un limpiado de la gráfica.");
                            }
                        }
                        
                        //Tabla Proceses
                        var Procesos = [];
                        var FlagPro;

                        //Si el número de gráficas es 1 obtengo los procesos para la selección
                        if (NumGraficas == 1) {

                            $.each(data.Table, function (i, j) {

                                if (FlagPro != j.PRO) {

                                    if (Procesos.length > 0) {

                                        Procesos[Procesos.length - 1].FFF = moment(j.FC).format("DD/MM/YYYY HH:mm:ss");
                                        Procesos[Procesos.length - 1].FF = j.FC;
                                    }

                                    FlagPro = j.PRO;

                                    Procesos.push({

                                        Desc: j.DPRO,
                                        FI: j.FC,
                                        FIF: moment(j.FC).format("DD/MM/YYYY HH:mm:ss"),
                                        FF: j.FC,
                                        FFF: j.FC
                                    });
                                }
                            });

                            //Último registro
                            Procesos[Procesos.length - 1].FFF = moment(data.Table[data.Table.length - 1].FC).format("DD/MM/YYYY HH:mm:ss");
                            Procesos[Procesos.length - 1].FF = data.Table[data.Table.length - 1].FC;
                        }

                        //Asignación a la tabla observable
                        self.aProcesos(Procesos);

                        //Creación de la gráfica
                        Plot_Consulta = $.plot(PlaceHolder, Data_Consulta, options);

                        //Tooltip
                        $("<div id='tooltip'></div>").css({
                            position: "absolute",
                            display: "none",
                            border: "1px solid #fdd",
                            padding: "2px",
                            "background-color": "#fee",
                            opacity: 0.80
                        }).appendTo("body");

                        //Muestra el tooltip en el punto seleccionado
                        PlaceHolder.bind("plothover", function (event, pos, item) {

                            if (item) {
                                var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);

                                var x_Time = parseInt(x);

                                $("#tooltip").html(item.series.label + ": " + y + " " + UnidadMedida + "<br />" + moment(x_Time).format("DD/MM/YYYY HH:mm:ss"))
                                    .css({ top: item.pageY + 5, left: item.pageX + 5 })
                                    .fadeIn(200);
                            } else {
                                $("#tooltip").hide();
                            }
                        });

                        self.SWGrafico(true);
                    } else {

                        MostrarMsjSA(3, "No se encontró Histórico para los datos ingresados o No se cuentan con los registros necesarios para graficar.");
                        self.SWGrafico(false);
                    }
                }
            });

            resp.fail(function (data) {

                MostrarMsjSA(3, "Los valores ingresados generaron una consulta con una gran cantidad de registros que son imposibles de procesar, por favor reduzca el rango de busqueda para continuar.");
                self.SWGrafico(false);
            });
        }
    }
    //#endregion

    //#region Seleccionar Procesos en gráfica
    self.CrearSeccion = function (item) {

        Plot_Consulta.clearSelection();

        Plot_Consulta.setSelection({
            xaxis: {
                from: new Date(item.FI).getTime(),
                to: new Date(item.FF).getTime()
            }
        });
    }
    //#endregion

    //#region Validar Formulario
    self.ValidarFormulario = function () {

        self.FechaIni = $("#txtFechaIni").val();
        self.HoraIni = $("#txtHoraIni").val();
        self.FechaFin = $("#txtFechaFin").val();
        self.HoraFin = $("#txtHoraFin").val();

        if (!self.TipoEquipo()) {

            MostrarMsjSA(3, "Debe Seleccionar un Tipo Equipo para continuar.");
            return false;
        } else if (!self.Equipo()) {

            MostrarMsjSA(3, "Debe Seleccionar un Equipo para continuar.");
            return false;
        } else if (!self.Variable()) {

            MostrarMsjSA(3, "Debe Seleccionar una Variable para continuar.");
            return false;
        } else if (!self.FechaIni) {

            MostrarMsjSA(3, "Debe Seleccioar una Fecha Inicial para continuar.");
            return false;
        } else if (!self.HoraIni) {

            MostrarMsjSA(3, "Debe Seleccioar una Hora Inicial para continuar.");
            return false;
        } else if (!self.FechaFin) {

            MostrarMsjSA(3, "Debe Seleccioar una Fecha Final para continuar.");
            return false;
        } else if (!self.HoraFin) {

            MostrarMsjSA(3, "Debe Seleccioar una Hora Final para continuar.");
            return false;
        }

        return true;
    }
    //#endregion

    //#region Exportar Datos
    self.ExportarDatos = function () {

        if (self.ValidarFormulario()) {

            window.open("/Reporting/ExportarExcel?Eq=" + self.Equipo().toString() +
                                               "&Var=" + self.Variable().toString() +
                                               "&FIni=" + self.FechaIni.split("/").join("_") +
                                               "&HIni=" + (self.HoraIni + ':00').split(":").join("_") +
                                               "&FFin=" + self.FechaFin.split("/").join("_") +
                                               "&HFin=" + (self.HoraFin + ':00').split(":").join("_") +
                                               "&NomVar=" + $("#ddlVariable").select2('data').text, "_top");
        }
    }
    //#endregion

    //#region Limpiar Formuario
    self.LimpiarForm = function () {

        $("#txtFechaIni").val(moment().format("DD/MM/YYYY"));
        $("#txtHoraIni").val('00:00');
        $("#txtFechaFin").val(moment().format("DD/MM/YYYY"));
        $("#txtHoraFin").val(moment().format("HH:mm"));

        self.TipoEquipo("");
        self.Equipo("");
        self.Variable("");
        self.SWGrafico(false);
        self.SWZoom(false);
        self.MsjZoom("");

        //Gráfica
        alreadyFetched = {};
        Data_Consulta = [];
        NumGraficas = 0;
        
        self.aProcesos([]);

        $(".Select2").trigger("change");

        self.LimpiarLimiteTemp();
    }
    //#endregion

    //#region Abrir Modal Limites
    self.AbrirModalLimites = function () {

        AbrirModal("modalRangos");
    }
    //#endregion

    //#region Limpiar Límite Temperatura
    self.LimpiarLimiteTemp = function () {

        self.LimiteSup("");
        self.LimiteInf("");
    }
    //#endregion

    //#region Zoom Grafica Proceso
    self.ZoomGraficoProceso = function (item) {

        self.SWZoom(true);

        self.ConsultarHistTempVol(item);
    }
    //#endregion

    //#region Generar PDF Auditoría
    self.GenerarPDFAuditoria = function () {

        self.FechaIni = $("#txtFechaIni").val();

        if (!self.TipoEquipo()) {

            MostrarMsjSA(3, "Debe Seleccionar un Tipo Equipo para continuar.");
            return false;
        } else if (!self.Equipo()) {

            MostrarMsjSA(3, "Debe Seleccionar un Equipo para continuar.");
            return false;
        } else if (!self.Variable()) {

            MostrarMsjSA(3, "Debe Seleccionar una Variable para continuar.");
            return false;
        } else if (!self.FechaIni) {

            MostrarMsjSA(3, "Debe Seleccioar una Fecha Inicial para continuar.");
            return false;
        }


        window.open("/Reporting/AuditoriaTemperatura?Fecha=" + self.FechaIni.split("/").join("_") +
                                                    "&Eq=" + self.Equipo().toString() +
                                                    "&Var=" + self.Variable().toString() +
                                                    "&NomEq=" + $("#ddlEquipo").select2('data').text, "_top");
    }
    //#endregion

    //#region ABC

    //#endregion

    //#region ABC

    //#endregion

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    //#region Sección

    //#region ABC

    //#endregion

    //#region ABC

    //#endregion

    //#endregion                                                                                                                                    Sección

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    //#region Sección                                                                                                                               Sección

    //#region ABC

    //#endregion

    //#region ABC

    //#endregion

    //#endregion

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//#endregion
}

var vm = new vmConsultas();
ko.applyBindings(vm);
//#endregion